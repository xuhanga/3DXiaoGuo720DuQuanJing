js版3D360度全景图/js版3D720度全景图

主要的应用有装饰装修网站

整个效果都是基于krpano.js,这款插件是收费的，不过价格人民币1000多点，一些扩展插件也需要花钱购买，不过对于一个公司来说不算什么，如果是商用！！！https://krpano.com/buy/

首页的index.html是官网案例的效果，每个demo文件夹存放写好的小案例，这些都是官网或者其他相关学习中整理的。
 
3D效果720度全景插件：  
https://krpano.com/    
中文教程：http://www.krpano360.com/   
方法接口归档：http://blog.csdn.net/u012054869/article/details/78394781  

效果被很多网站使用：  
https://720.3vjia.com/S1CKGICFC   
http://www.yunjing720.com/t/aa862a47ec116a76   
https://yun.kujiale.com/design/3FO4M8GLK9T9/show   


微信sdk(如果手机微信使用，多配合分享处理)：  
http://qydev.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1JS-SDK%E6%8E%A5%E5%8F%A3